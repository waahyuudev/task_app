import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Alert,
    ActivityIndicator,
    TouchableOpacity,
} from 'react-native';
import { Colors } from 'react-native-paper';
import { AppStyles } from '../../app_styles';
import Toast from 'react-native-simple-toast';
import { request, PERMISSIONS } from 'react-native-permissions';


const LoginPage = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    var SharedPreferences = require("react-native-shared-preferences");


    const onPressLogin = async () => {
        try {
            setLoading(true)
            const response = await fetch('http://task-service.fun/api/login', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            });

            const json = await response.json()

            console.log(`response ${JSON.stringify(json)}`)

            if (json.code == 200) {
                //                console.log("token : " + json.data.remember_token);

                if (json.data.role === "USER")
                    SharedPreferences.setItem("isUser", "true");
                else
                    SharedPreferences.setItem("isUser", "false");

                SharedPreferences.setName("credential");
                SharedPreferences.setItem("token", json.data.remember_token);
                SharedPreferences.setItem("user", JSON.stringify(json.data));
                navigation.replace("MainPage", { user: json.data })
            } else {
                Toast.show(json.message, Toast.LONG);
            }

        } catch (error) {
            console.log(`error ${error} runtime type ${typeof (error)}`)
        } finally {
            setLoading(false)
        }
    };

    useEffect(() => {
        request(Platform.OS === 'ios' ? PERMISSIONS.IOS.WRITE_EXTERNAL_STORAGE : PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE).then((result) => {
            console.log(result)
        });
        request(Platform.OS === 'ios' ? PERMISSIONS.IOS.READ_EXTERNAL_STORAGE : PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE).then((result) => {
            console.log(result)
        });
        request(Platform.OS === 'ios' ? PERMISSIONS.IOS.CAMERA : PERMISSIONS.ANDROID.CAMERA).then((result) => {
            console.log(result)
        });
        SharedPreferences.setName("credential");
        SharedPreferences.getItem("token", function (value) {
            //            console.log("token " + value)
            if (value != null) {
                navigation.replace("MainPage")
            }
        });
    });

    return (
        <View style={styles.container}>
            <Text style={[styles.title, styles.leftTitle]}>Sign In</Text>
            <View style={styles.InputContainer}>
                <TextInput
                    style={styles.body}
                    placeholder="E-mail or phone number"
                    onChangeText={setEmail}
                    value={email}
                    placeholderTextColor={AppStyles.color.grey}
                    underlineColorAndroid="transparent"
                />
            </View>
            <View style={styles.InputContainer}>
                <TextInput
                    style={styles.body}
                    secureTextEntry={true}
                    placeholder="Password"
                    onChangeText={setPassword}
                    value={password}
                    placeholderTextColor={AppStyles.color.grey}
                    underlineColorAndroid="transparent"
                />
            </View>
            <TouchableOpacity
                style={styles.loginContainer}
                onPress={() => onPressLogin()}>
                <Text style={styles.loginText}>Log in</Text>
            </TouchableOpacity>
            {loading ? <ActivityIndicator style={{ marginTop: 30 }}
                size="large"
                animating={loading}
                color={AppStyles.color.tint} /> : <View></View>}
            <TouchableOpacity onPress={() => navigation.navigate('RegisterPage')}>
                <Text style={styles.signUpText}>
                    Sign Up
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    or: {
        color: 'black',
        marginTop: 40,
        marginBottom: 10,
    },
    title: {
        fontSize: AppStyles.fontSize.title,
        fontWeight: 'bold',
        color: AppStyles.color.tint,
        marginTop: 20,
        marginBottom: 20,
    },
    leftTitle: {
        alignSelf: 'stretch',
        textAlign: 'left',
        marginLeft: 20,
    },
    content: {
        paddingLeft: 50,
        paddingRight: 50,
        textAlign: 'center',
        fontSize: AppStyles.fontSize.content,
        color: AppStyles.color.text,
    },
    loginContainer: {
        alignItems: 'center',
        width: AppStyles.buttonWidth.main,
        backgroundColor: AppStyles.color.tint,
        borderRadius: AppStyles.borderRadius.main,
        padding: 10,
        marginTop: 30,
    },
    loginText: {
        color: AppStyles.color.white,
    },
    placeholder: {
        color: 'red',
    },
    InputContainer: {
        width: AppStyles.textInputWidth.main,
        marginTop: 30,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: AppStyles.color.grey,
        borderRadius: AppStyles.borderRadius.main,
    },
    body: {
        height: 42,
        paddingLeft: 20,
        paddingRight: 20,
        color: AppStyles.color.text,
    },
    facebookContainer: {
        alignItems: 'center',
        width: 192,
        backgroundColor: AppStyles.color.facebook,
        borderRadius: AppStyles.borderRadius.main,
        padding: 10,
        marginTop: 30,
    },
    facebookText: {
        color: AppStyles.color.white,
    },
    googleContainer: {
        width: 192,
        height: 48,
        marginTop: 30,
    },
    googleText: {
        color: AppStyles.color.white,
    },
    signUpText: {
        marginTop: 10,
        color: Colors.blue500
    }
});

export default LoginPage;