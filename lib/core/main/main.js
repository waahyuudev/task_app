import React, { useState, useEffect } from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Task from '../task/task';
import ProfilePage from '../profile/profile'
import { AppStyles } from '../../app_styles';

const Tab = createMaterialBottomTabNavigator();

const MainPage = () => {

    return (<Tab.Navigator
        initialRouteName="Home"
        activeColor={AppStyles.color.tint}
        barStyle={{ backgroundColor: 'white', paddingBottom: 5, paddingTop: 5 }}
    >
        <Tab.Screen
            name="Task"
            component={Task}
            options={{
                tabBarLabel: 'Kegiatan',
                tabBarIcon: ({ color }) => (
                    <MaterialCommunityIcons name="home-outline" color={color} size={26} />
                ),
            }}
        />
        <Tab.Screen
            name="Profile"
            component={ProfilePage}
            options={{
                tabBarLabel: 'Profile',
                tabBarIcon: ({ color }) => (
                    <MaterialCommunityIcons name="account-details-outline" color={color} size={26} />
                ),
            }}
        />


    </Tab.Navigator>)
}

export default MainPage