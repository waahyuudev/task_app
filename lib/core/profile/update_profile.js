import React, { useEffect, useState, useCallback } from 'react';
import { Appbar } from 'react-native-paper';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ActivityIndicator,
    TouchableOpacity,
    Image,
    ImageBackground
} from 'react-native';
import { AppStyles } from '../../app_styles';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as ImagePicker from "react-native-image-picker";
import ImgToBase64 from "react-native-image-base64";
import Toast from 'react-native-simple-toast';
import { ScrollView } from 'react-native-gesture-handler';
import DocumentPicker from 'react-native-document-picker';

const UpdateProfilePage = ({ navigation }) => {

    const [user, setUser] = useState({});
    let uri = null;
    const [token, setToken] = useState('');
    const [pickerResponse, setPickerResponse] = useState(null);
    const [isLoading, setLoading] = useState(false);
    const [fullname, setFullname] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    var SharedPreferences = require("react-native-shared-preferences");

    const onImageLibraryPress = useCallback(() => {
        //    setVisible(false);
        const options = {
            selectionLimit: 1,
            mediaType: "photo",
            includeBase64: false
        };
        ImagePicker.launchImageLibrary(options, setPickerResponse);
    }, []);

    if (pickerResponse != null) {
        console.log("pickerResponse.assets : " + pickerResponse.assets);
        console.log(
            "pickerResponse.assets[0].uri : " + pickerResponse.assets[0].uri
        );
        uri = pickerResponse.assets && pickerResponse.assets[0].uri;
    }

    const updateProfile = async () => {
        try {
            // console.log("reqApi : " + base64String.substring(0, 4));
            var headers = {
                'Authorization': 'Bearer ' + token,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            };

            const response = await fetch('http://task-service.fun/api/update_user', {
                method: 'POST',
                headers: headers,
                body: JSON.stringify({
                    user_id: user.user_id.toString(),
                    name: fullname,
                    email: email,
                    phone_number: phone,
                    password: password
                    // image: base64String
                })
            });
            const json = await response.json()
            if (json.code == 200) {
                console.log("resp " + JSON.stringify(json))
                console.log("header " + JSON.stringify(headers))
                navigation.navigate("MainPage")
            } else {
                console.log("resp " + json.message);
                Toast.show(json.message, Toast.LONG);
            }
        } catch (error) {
            console.log(`error ${error} runtime type ${typeof (error)}`)
        } finally {
            setLoading(false);
        }
    }

    const generateBase64Image = (uriImage) => {
        if (uriImage != null) {
            ImgToBase64.getBase64String(uriImage)
                .then(base64String => {
                    console.log("image generated : " + base64String.substring(0, 4));
                    // reqApi(base64String);
                    updateProfile(base64String);

                })
                .catch(err => console.log("error " + err));
        } else {
            writeUserData(packageImage);
        }
    };

    const onPressUpdate = async () => {
        try {
            setLoading(true)
            // generateBase64Image(uri);
            // todo for hit service

            updateProfile();

        } catch (error) {
            console.log(`error ${error} runtime type ${typeof (error)}`)
        } finally {
            setLoading(false);
        }
    }

    const setCurrentValue = () => {
        setFullname(user.name ? user.name : '')
        setEmail(user.email ? user.email : '')
        setPhone(user.phone_number ? user.phone_number : '')
    }

    useEffect(() => {
        SharedPreferences.setName("credential");
        SharedPreferences.getItem("token", function (value) {
            setToken(value)
        });
        SharedPreferences.getItem("user", function (value) {
            console.log(`user ${value}`)
            if (value != null) {
                setUser(JSON.parse(value))
                setCurrentValue()
            }

        });
    }, []);

    return (<View style={{ flex: 1 }}>
        <Appbar.Header style={{ backgroundColor: AppStyles.color.tint }}>
            <Appbar.BackAction onPress={() => { navigation.goBack() }} color={AppStyles.color.white} />
            <Appbar.Content title="Update Profile" titleStyle={{ color: AppStyles.color.white }} />
        </Appbar.Header>
        <ScrollView>
            <View style={styles.container}>

                {/* <ImageBackground style={styles.imageBackground}>
                    <Image
                        style={{
                            height: 200,
                            width: 200,
                            overflow: "visible",
                            marginBottom: 20,
                            marginTop: 20
                        }}
                        source={
                            uri != undefined
                                ? { uri: uri }
                                : require("../../../assets/profile.png")
                        }
                    />
                </ImageBackground>
                <TouchableOpacity
                    mode="contained"
                    onPress={onImageLibraryPress}
                    style={[styles.buttonStyle, { marginBottom: 30 }]}
                >
                    <Text style={styles.txtStyleButton}>Upload Photo</Text>
                </TouchableOpacity> */}

                <View style={styles.InputContainer}>
                    <TextInput
                        style={styles.body}
                        placeholder="Full Name"
                        onChangeText={setFullname}
                        value={fullname}
                        placeholderTextColor={AppStyles.color.grey}
                        underlineColorAndroid="transparent"
                    />
                </View>
                <View style={styles.InputContainer}>
                    <TextInput
                        style={styles.body}
                        placeholder="Phone Number"
                        onChangeText={setPhone}
                        value={phone}
                        placeholderTextColor={AppStyles.color.grey}
                        underlineColorAndroid="transparent"
                    />
                </View>
                <View style={styles.InputContainer}>
                    <TextInput
                        style={styles.body}
                        placeholder="E-mail Address"
                        onChangeText={setEmail}
                        value={email}
                        placeholderTextColor={AppStyles.color.grey}
                        underlineColorAndroid="transparent"
                    />
                </View>
                <View style={styles.InputContainer}>
                    <TextInput
                        style={styles.body}
                        placeholder="Password"
                        secureTextEntry={true}
                        onChangeText={setPassword}
                        value={password}
                        placeholderTextColor={AppStyles.color.grey}
                        underlineColorAndroid="transparent"
                    />
                </View>
                <TouchableOpacity
                    style={[styles.loginContainer, { marginBottom: 30 }]}
                    onPress={() => onPressUpdate()}>
                    {isLoading ? <ActivityIndicator /> : <Text style={[styles.loginText, { textAlign: 'center' }]}>Update</Text>}
                </TouchableOpacity>
            </View>
        </ScrollView>
    </View>)
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    title: {
        fontSize: AppStyles.fontSize.title,
        fontWeight: 'bold',
        color: AppStyles.color.tint,
        marginTop: 20,
        marginBottom: 20,
    },
    leftTitle: {
        alignSelf: 'stretch',
        textAlign: 'left',
        marginLeft: 20,
    },
    content: {
        paddingLeft: 50,
        paddingRight: 50,
        textAlign: 'center',
        fontSize: AppStyles.fontSize.content,
        color: AppStyles.color.text,
    },
    loginContainer: {
        width: AppStyles.buttonWidth.main,
        backgroundColor: AppStyles.color.tint,
        borderRadius: AppStyles.borderRadius.main,
        padding: 10,
        marginTop: 30,
    },
    loginText: {
        color: AppStyles.color.white,
    },
    placeholder: {
        color: 'red',
    },
    InputContainer: {
        width: AppStyles.textInputWidth.main,
        marginTop: 30,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: AppStyles.color.grey,
        borderRadius: AppStyles.borderRadius.main,
    },
    body: {
        height: 42,
        paddingLeft: 20,
        paddingRight: 20,
        color: AppStyles.color.text,
    },
    buttonStyle: {
        marginRight: 24,
        marginLeft: 24,
        marginTop: 10,
        marginBottom: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#009EFF',
    },
    txtStyleButton: {
        color: '#009EFF',
        width: 250,
        textAlign: 'center',
    },
});

export default UpdateProfilePage