import React, { useEffect, useState } from 'react';
import { Appbar } from 'react-native-paper';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ActivityIndicator,
    TouchableOpacity,
    Image,
} from 'react-native';
import ProfileStyle from './profile_style'
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const ProfilePage = ({ navigation }) => {

    const [name, setPresentName] = useState("");
    const [email, setPresentEmail] = useState("");
    const [phone, setPresentPhone] = useState("");
    const [userImage, setUserImage] = useState("");
    const [user, setUser] = useState({});
    var SharedPreferences = require("react-native-shared-preferences");

    const _getImageUri = (base64) => {
        let imageUri = "";
        console.log("userImageProfileScreen : " + base64);
        if (base64 != undefined || base64 != '' || base64 != "null") {
            imageUri = "data:image/png;base64," + base64;
        }
        return imageUri
    }

    const onPressLogout = () => {
        SharedPreferences.clear();
        navigation.navigate("LoginPage")
    }

    const onPressUpdateProfile = () => {
        navigation.navigate("UpdateProfilePage")
    }

    useEffect(() => {
        SharedPreferences.setName("credential");
        SharedPreferences.getItem("user", function (value) {
            if (value != null)
                setUser(JSON.parse(value))
        });
    });



    return (<View style={{ flex: 1 }}>
        <Appbar.Header style={{ backgroundColor: "#000080" }}>
            <Appbar.Content title="Profile" titleStyle={{ color: "white" }} />
            <Appbar.Action icon="logout" onPress={() => onPressLogout()} color={"white"} />

        </Appbar.Header>

        <View style={{ flex: 1 }}>
            <View style={styles.header} />
            <View style={styles.body}>
                <View style={styles.bodyContent}>
                    <Text style={styles.txtName}>
                        {user.name ? user.name : "Default"}
                    </Text>
                    <Text style={styles.info}>
                        <MaterialCommunityIcons
                            name="email"
                            color={"#575758"}
                            size={26}
                        />
                        {user.email ? user.email : "Default"}
                    </Text>
                    <Text style={styles.info}>
                        <MaterialCommunityIcons
                            name="whatsapp"
                            color={"#575758"}
                            size={26}
                        />
                        {user.phone_number ? user.phone_number : "Default"}
                    </Text>
                    <TouchableOpacity style={[styles.buttonStyle, { marginTop: 20 }]} onPress={() => onPressUpdateProfile()}>
                        <Text style={{ color: 'black' }}>
                            Update Profile
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
            <Image
                style={styles.avatar}
                source={(user.avatar != "") ? { uri: _getImageUri(user.avatar) } : require("../../../assets/profile.png")}
            />

        </View>

    </View>)
}

const styles = ProfileStyle



export default ProfilePage