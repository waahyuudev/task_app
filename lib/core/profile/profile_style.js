import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFD0EC"
  },
  containerWhite: {
    flex: 1,
    backgroundColor: "#FFFFFF"
  },
  header: {
    height: 200
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 30,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
    alignSelf: "center",
    position: "absolute",
    marginTop: 150
  },
  txtName: {
    fontSize: 16,
    color: "#575758",
    fontWeight: "600",
    marginTop: 20
  },
  body: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    marginTop: 40,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0
  },
  bodyContent: {
    alignItems: "center",
    padding: 30
  },
  name: {
    fontSize: 28,
    color: "#696969",
    fontWeight: "600"
  },
  info: {
    fontSize: 16,
    color: "#575758",
    marginTop: 5
  },
  description: {
    fontSize: 16,
    color: "#696969",
    marginTop: 10,
    textAlign: "center"
  },
  buttonContainer: {
    marginTop: 30,
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    backgroundColor: "#fff"
  },
  textButton: {
    color: "#09101D"
  },
  buttonStyle: {
    marginRight: 40,
    marginLeft: 40,
    marginTop: 10,
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: "#fff",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#000080"
  },
  txtStyleButton: {
    color: "#FFD0EC",
    alignSelf: 'baseline',
    textAlign: "center"
  },
});