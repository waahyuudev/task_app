import React, { useEffect, useState } from 'react';
import { Appbar } from 'react-native-paper';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    KeyboardAvoidingView,
    Image,
    ImageBackground
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { AppStyles } from '../../app_styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CommentItem from '../../components/report_item';
import RNFetchBlob from "rn-fetch-blob";
import RNFS from 'react-native-fs';
import Toast from 'react-native-simple-toast';

const DetailReportPage = ({ route, navigation }) => {

    const data = route.params.detailReport;
    console.log("data report " + JSON.stringify(data))
    const [token, setToken] = useState('');
    const [comment, setComment] = useState('');
    var SharedPreferences = require("react-native-shared-preferences");


    const onPressAddComment = async () => {
        try {
            var headers = {
                'Authorization': 'Bearer ' + token,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            };
            const response = await fetch('http://task-service.fun/api/add_comment', {
                method: 'POST',
                headers: headers,
                body: JSON.stringify({
                    report_id: data.id.toString(),
                    content: comment
                })
            });
            const json = await response.json()
            console.log("resp " + JSON.stringify(json))
            console.log("header " + JSON.stringify(headers))
            navigation.navigate("MainPage")
        } catch (error) {
            console.log(`error ${error} runtime type ${typeof (error)}`)
        }
    }

    const onPressUpdate = () => {
        navigation.navigate("UpdateTaskPage", { detailTask: data })
    }

    const onPressDelete = async (id) => {
        try {
            var headers = {
                'Authorization': 'Bearer ' + token,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            };
            const response = await fetch('http://task-service.fun/api/delete_task', {
                method: 'POST',
                headers: headers,
                body: JSON.stringify({
                    id: id
                })
            });
            const json = await response.json()
            if (json.code == 200) {
                console.log(`response ${JSON.stringify(json)}`)
                navigation.navigate('TaskPage')
            } else {
                Toast.show(json.message, Toast.LONG);
            }
        } catch (error) {
            console.log(`error ${error} runtime type ${typeof (error)}`)
        }
    }

    const onPressDownloadReport = () => {
        console.log("download " + "http://task-service.fun/" + data.file)
        const getFileName = data.file.substring(data.file.lastIndexOf('/') + 1)
        console.log('getFileName ' + getFileName)
        RNFetchBlob.config({
            // add this option that makes response data to be stored as a file,
            // this is much more performant.
            fileCache: true,
        })
            .fetch("GET", "http://task-service.fun/" + data.file, {
                //some headers ..
            })
            .then(async (res) => {
                var today = Math.round((new Date()).getTime() / 1000);
                // the temp file path
                console.log("The file saved to ", res.path());
                console.log("The file type ", res.type);
                console.log("res download ", JSON.stringify(res));
                await RNFS.copyFile(res.path(), RNFS.DownloadDirectoryPath + "/" + `LAPORAN_${getFileName}`)
                Toast.show("Berhasil Download Laporan", Toast.LONG);
            })
            // Something went wrong:
            .catch((errorMessage, statusCode) => {
                // error handling
                console.log('error ' + errorMessage)
            });
    }

    useEffect(() => {
        SharedPreferences.setName("credential");
        SharedPreferences.getItem("token", function (value) {
            setToken(value)
        });
    }, []);

    return (
        <View style={{ flex: 1 }}>
            <Appbar.Header style={{ backgroundColor: AppStyles.color.tint }}>
                <Appbar.BackAction onPress={() => { navigation.goBack() }} color={AppStyles.color.white} />
                <Appbar.Content title={data.name} titleStyle={{ color: AppStyles.color.white }} />
                <Appbar.Action icon="square-edit-outline" onPress={() => onPressUpdate()} color={AppStyles.color.white} />
                <Appbar.Action icon="trash-can-outline" onPress={() => onPressDelete(data.id.toString())} color={AppStyles.color.white} />
            </Appbar.Header>
            <View style={styles.container}>
                <ImageBackground style={styles.imageBackground}>
                    <Image style={{
                        height: 200,
                        width: 200,
                        overflow: "visible",
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginBottom: 20
                    }}
                        source={data.image === "null"
                            ? { uri: require("../../../assets/profile.png") }
                            : { uri: data.image }}
                    />
                </ImageBackground>
                <View>
                    <View style={{ marginTop: 15 }}><Text style={styles.subTitle}>Tanggal Laporan</Text></View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginTop: 5, flex: 1 }}><Text style={styles.regular}>{data.date}</Text></View>
                        <View style={{ marginTop: 5 }}><TouchableOpacity
                            onPress={() => onPressDownloadReport()}>
                            <Text style={[styles.regular, { color: 'blue' }]}>| Unduh Laporan</Text>
                        </TouchableOpacity></View>
                    </View>
                </View>
                <View>
                    <View style={{ marginTop: 15 }}><Text style={styles.subTitle}>Deskripsi</Text></View>
                    <View style={{ marginTop: 5 }}><Text style={styles.regular}>{data.description}</Text></View>
                </View>
                <View style={{ flex: 1 }}>
                    <View style={{}}><Text style={styles.subTitle}>Komentar :</Text></View>
                    <FlatList style={{ marginLeft: 12, marginTop: 7 }} data={data.comments}
                        keyExtractor={({ id }, index) => id}
                        renderItem={({ item }) => (

                            <View style={{ flexDirection: 'row', alignSelf: 'baseline', alignItems: 'center', justifyContent: "center", marginBottom: 10 }}>
                                <Text style={[{ marginRight: 10, fontWeight: 'bold', fontSize: 12, color: 'black' }]}>[ {item.user.name} ]</Text>
                                <Text style={{ fontSize: 12, color: 'black' }}>{item.content}</Text>
                            </View>

                        )}
                    />
                </View>
                {<View style={{ flex: 1 }}>
                    <KeyboardAvoidingView
                        behavior={Platform.OS === "ios" ? "padding" : "height"}
                        style={styles.writeTaskWrapper}
                    >
                        <TextInput style={[{ flex: 1, marginRight: 20 }, styles.input]} placeholder={'type comment..'} value={comment} onChangeText={text => setComment(text)} />
                        <TouchableOpacity onPress={() => onPressAddComment()}>
                            <Ionicons name='send' size={20} color={'black'} />
                        </TouchableOpacity>
                    </KeyboardAvoidingView>
                </View>}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E8EAED',
        padding: 16,
        flexDirection: 'column'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18
    },
    subTitle: {
        fontWeight: '500',
        fontSize: 16,
        marginTop: 15,
        color: 'black'
    },
    regular: {
        fontSize: 14,
        color: 'black'
    },

    buttonStyle: {
        marginRight: 24,
        marginLeft: 24,
        marginTop: 10,
        marginBottom: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: AppStyles.color.tint,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: AppStyles.color.tint,
    },
    txtStyleButton: {
        color: AppStyles.color.white,
        width: 250,
        textAlign: 'center',
    },
    buttonStyleOutline: {
        marginRight: 24,
        marginLeft: 24,
        marginBottom: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: AppStyles.color.white,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: AppStyles.color.tint,
    },
    txtStyleButtonOutline: {
        color: AppStyles.color.tint,
        width: 250,
        textAlign: 'center',
    },
    writeTaskWrapper: {
        padding: 10,
        position: 'absolute',
        bottom: 10,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        paddingVertical: 5,
        paddingHorizontal: 20,
        backgroundColor: '#FFF',
        borderRadius: 10,
        color: '#000',
        borderColor: '#C0C0C0',
        borderWidth: 1,
    },
    addWrapper: {
        width: 30,
        height: 30,
        backgroundColor: '#FFF',
        borderRadius: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#C0C0C0',
        marginLeft: 10,
        borderWidth: 1,
    },
    item: {
        backgroundColor: '#FFF',
        padding: 15,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 20,
    },
    addText: {},
});

export default DetailReportPage