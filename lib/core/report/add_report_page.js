import React, { useEffect, useCallback, useState } from 'react';
import { Appbar } from 'react-native-paper';
import {
    StyleSheet,
    Text,
    TextInput,
    Image,
    View,
    ActivityIndicator,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';
import { AppStyles } from '../../app_styles';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as ImagePicker from "react-native-image-picker";
import ImgToBase64 from "react-native-image-base64";
import Toast from 'react-native-simple-toast';
import { ScrollView } from 'react-native-gesture-handler';
import DocumentPicker from 'react-native-document-picker';

const AddReportPage = ({ route, navigation }) => {
    let uri = null;
    const [pickerResponse, setPickerResponse] = useState(null);
    const [isLoading, setLoading] = useState(false);
    const [token, setToken] = useState('');
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [mydate, setDate] = useState(new Date());
    const [isDisplayDate, setDisplayDate] = useState(false);
    var SharedPreferences = require("react-native-shared-preferences");
    const data = route.params.detailTask;
    const [singleFile, setSingleFile] = useState(null);


    const uploadReport = async (base64String) => {
        try {
            // Check if any file is selected or not
            if (singleFile != null) {
                // If file selected then create FormData
                const fileToUpload = singleFile;
                const formData = new FormData();
                formData.append("task_id", data.id.toString());
                formData.append("name", name);
                formData.append("date", mydate.toLocaleDateString());
                formData.append("description", description);
                formData.append('file', {
                    name: singleFile.name,
                    type: singleFile.type,
                    uri: Platform.OS === 'ios' ?
                        singleFile.uri.replace('file://', '')
                        : singleFile.uri,
                });
                formData.append('image', base64String);
                // Please change file upload URL
                let res = await fetch(
                    'http://task-service.fun/api/add_report',
                    {
                        method: 'post',
                        body: formData,
                        headers: {
                            'Authorization': 'Bearer ' + token,
                            'Content-Type': 'multipart/form-data; '
                        },
                    }
                );
                console.log(`res api ${JSON.stringify(res)}`)
                let responseJson = await res.json();
                if (responseJson.status == 1) {
                    // alert('Upload Successful');
                    navigation.navigate("MainPage")
                } else {
                    console.log("resp " + json.message);
                    Toast.show(json.message, Toast.LONG);
                }
            } else {
                // If no file selected the show alert
                alert('Please Select File first');
            }
        } catch (error) {
            console.log(`error ${error} runtime type ${typeof (error)}`)
        } finally {
            setLoading(false);
        }
    };

    const selectFile = async () => {
        // Opening Document Picker to select one file
        try {
            const res = await DocumentPicker.pick({
                // Provide which type of file you want user to pick
                type: [DocumentPicker.types.allFiles, DocumentPicker.types.images, DocumentPicker.types.plainText, DocumentPicker.types.audio, DocumentPicker.types.pdf],
                // There can me more options as well
                // DocumentPicker.types.allFiles
                // DocumentPicker.types.images
                // DocumentPicker.types.plainText
                // DocumentPicker.types.audio
                // DocumentPicker.types.pdf
            });
            // Printing the log realted to the file
            console.log('res : ' + JSON.stringify(res));
            // Setting the state to show single file attributes
            setSingleFile(res[0]);
        } catch (err) {
            setSingleFile(null);
            // Handling any exception (If any)
            if (DocumentPicker.isCancel(err)) {
                // If user canceled the document selection
                alert('Canceled');
            } else {
                // For Unknown Error
                alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    }


    const changeSelectedDate = (event, selectedDate) => {
        const currentDate = selectedDate || mydate;
        setDate(currentDate);
        setDisplayDate(false)
        console.log("localdate " + mydate.toLocaleDateString())
    };

    const displayDatepicker = () => {
        setDisplayDate(true);
    };

    const generateBase64Image = (uriImage) => {
        if (uriImage != null) {
            ImgToBase64.getBase64String(uriImage)
                .then(base64String => {
                    console.log("image generated : " + base64String.substring(0, 4));
                    // reqApi(base64String);
                    uploadReport(base64String);

                })
                .catch(err => console.log("error " + err));
        } else {
            writeUserData(packageImage);
        }
    };
    const onImageLibraryPress = useCallback(() => {
        //    setVisible(false);
        const options = {
            selectionLimit: 1,
            mediaType: "photo",
            includeBase64: false
        };
        ImagePicker.launchImageLibrary(options, setPickerResponse);
    }, []);

    if (pickerResponse != null) {
        console.log("pickerResponse.assets : " + pickerResponse.assets);
        console.log(
            "pickerResponse.assets[0].uri : " + pickerResponse.assets[0].uri
        );
        uri = pickerResponse.assets && pickerResponse.assets[0].uri;
    }

    const onPressAdd = async () => {
        try {
            setLoading(true)
            generateBase64Image(uri);
            // todo for hit service


        } catch (error) {
            console.log(`error ${error} runtime type ${typeof (error)}`)
        } finally {
            setLoading(false);
        }
    }

    // const reqApi = async (base64String) => {
    //     try {
    //         console.log("reqApi : " + base64String.substring(0, 4));
    //         var headers = {
    //             'Authorization': 'Bearer ' + token,
    //             Accept: 'application/json',
    //             'Content-Type': 'application/json'
    //         };

    //         const response = await fetch('http://task-service.fun/api/add_report', {
    //             method: 'POST',
    //             headers: headers,
    //             body: JSON.stringify({
    //                 task_id: data.id.toString(),
    //                 user_id: data.user_id.toString(),
    //                 name: name,
    //                 description: description,
    //                 date: mydate.toLocaleDateString(),
    //                 image: base64String
    //             })
    //         });
    //         const json = await response.json()
    //         if (json.code == 200) {
    //             console.log("resp " + JSON.stringify(json))
    //             console.log("header " + JSON.stringify(headers))
    //             navigation.navigate("TaskPage")
    //         } else {
    //             console.log("resp " + json.message);
    //             Toast.show(json.message, Toast.LONG);
    //         }
    //     } catch (error) {
    //         console.log(`error ${error} runtime type ${typeof (error)}`)
    //     } finally {
    //         setLoading(false);
    //     }
    // }

    // const addReport = () => {
    //     var myHeaders = new Headers();
    //     myHeaders.append("Authorization", "Bearer K5c4e44Da10F6u584vh2tAt5RIVhEe");

    //     var formdata = new FormData();
    //     formdata.append("task_id", data.id.toString());
    //     formdata.append("name", name);
    //     formdata.append("date", mydate.toLocaleDateString());
    //     formdata.append("description", description);
    //     formdata.append("file", fileInput.files[0], "module.sql");

    //     var requestOptions = {
    //         method: 'POST',
    //         headers: myHeaders,
    //         body: formdata,
    //     };

    //     fetch(" http://task-service.fun/api/add_report", requestOptions)
    //         .then(response => response.text())
    //         .then(result => console.log(result))
    //         .catch(error => console.log('error', error));

    // }

    useEffect(() => {
        SharedPreferences.setName("credential");
        SharedPreferences.getItem("token", function (value) {
            setToken(value)
        });
    }, []);

    return (<View style={{ flex: 1 }}>

        <Appbar.Header style={{ backgroundColor: AppStyles.color.tint }}>
            <Appbar.BackAction onPress={() => { navigation.goBack() }} color={AppStyles.color.white} />
            <Appbar.Content title="Tambah Laporan" titleStyle={{ color: AppStyles.color.white }} />
        </Appbar.Header>
        <ScrollView>
            <View style={styles.container}>
                <ImageBackground style={styles.imageBackground}>
                    <Image
                        style={{
                            height: 200,
                            width: 200,
                            overflow: "visible",
                            marginBottom: 20
                        }}
                        source={
                            uri != undefined
                                ? { uri: uri }
                                : require("../../../assets/default.jpeg")
                        }
                    />
                </ImageBackground>
                <TouchableOpacity
                    mode="contained"
                    onPress={onImageLibraryPress}
                    style={[styles.buttonStyle, { marginBottom: 30 }]}
                >
                    <Text style={styles.txtStyleButton}>Upload Photo Kegiatan</Text>
                </TouchableOpacity>
                <View style={styles.InputContainer}>
                    <TextInput
                        style={styles.body}
                        placeholder="Name"
                        onChangeText={setName}
                        value={name}
                        placeholderTextColor={AppStyles.color.grey}
                        underlineColorAndroid="transparent"
                    />
                </View>
                <View style={styles.InputContainer}>
                    <TextInput
                        style={styles.body}
                        placeholder="Description"
                        onChangeText={setDescription}
                        value={description}
                        placeholderTextColor={AppStyles.color.grey}
                        underlineColorAndroid="transparent"
                    />
                </View>
                <Text style={{ marginTop: 20, marginBottom: 10, color: 'black' }}>Select Date</Text>
                {isDisplayDate ? <DateTimePicker
                    testID="dateTimePicker"
                    value={mydate}
                    mode={'date'}
                    is24Hour={true}
                    display="default"
                    onChange={changeSelectedDate}
                /> : <TouchableOpacity
                    onPress={() => displayDatepicker()}
                    style={{
                        paddingHorizontal: 20,
                        paddingVertical: 10,
                        borderWidth: 1,
                        borderStyle: 'solid',
                        borderColor: AppStyles.color.grey,
                        borderRadius: 12,
                        width: AppStyles.buttonWidth.main,
                        alignContent: 'center',
                        alignItems: 'center'
                    }}>
                    <Text style={{ color: 'black' }}>{mydate.toLocaleDateString()}</Text>
                </TouchableOpacity>}

                <Text style={{ marginTop: 20, marginBottom: 10, color: 'black' }}>Select File</Text>

                <TouchableOpacity
                    style={{
                        paddingHorizontal: 20,
                        paddingVertical: 10,
                        borderWidth: 1,
                        borderStyle: 'solid',
                        borderColor: AppStyles.color.grey,
                        borderRadius: 12,
                        width: AppStyles.buttonWidth.main,
                        alignContent: 'center',
                        alignItems: 'center'
                    }}
                    activeOpacity={0.5}
                    onPress={selectFile}>
                    <Text style={{ color: 'black' }}>Select File</Text>
                </TouchableOpacity>
                {singleFile != null ? (
                    <Text style={{ color: 'black', marginTop: 20 }}>
                        File Name: {singleFile.name ? singleFile.name : ''}
                    </Text>
                ) : null}


                <TouchableOpacity
                    style={[styles.loginContainer, { marginBottom: 30 }]}
                    onPress={() => onPressAdd()}>
                    {isLoading ? <ActivityIndicator /> : <Text style={styles.loginText}>Add</Text>}
                </TouchableOpacity>
            </View>
        </ScrollView>

    </View>)

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    title: {
        fontSize: AppStyles.fontSize.title,
        fontWeight: 'bold',
        color: AppStyles.color.tint,
        marginTop: 20,
        marginBottom: 20,
    },
    leftTitle: {
        alignSelf: 'stretch',
        textAlign: 'left',
        marginLeft: 20,
    },
    content: {
        paddingLeft: 50,
        paddingRight: 50,
        textAlign: 'center',
        fontSize: AppStyles.fontSize.content,
        color: AppStyles.color.text,
    },
    loginContainer: {
        alignItems: 'center',
        width: AppStyles.buttonWidth.main,
        backgroundColor: AppStyles.color.tint,
        borderRadius: AppStyles.borderRadius.main,
        padding: 10,
        marginTop: 30,
    },
    loginText: {
        color: AppStyles.color.white,
    },
    placeholder: {
        color: 'red',
    },
    InputContainer: {
        width: AppStyles.textInputWidth.main,
        marginTop: 30,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: AppStyles.color.grey,
        borderRadius: AppStyles.borderRadius.main,
    },
    body: {
        height: 42,
        paddingLeft: 20,
        paddingRight: 20,
        color: AppStyles.color.text,
    },
    buttonStyle: {
        marginRight: 24,
        marginLeft: 24,
        marginTop: 10,
        marginBottom: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#009EFF',
    },
    txtStyleButton: {
        color: '#009EFF',
        width: 250,
        textAlign: 'center',
    },
});

export default AddReportPage;