import React, { useState } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ActivityIndicator,
    TouchableOpacity,
} from 'react-native';
import { Colors } from 'react-native-paper';
import { AppStyles } from '../../app_styles';
import Toast from 'react-native-simple-toast';

const RegisterPage = ({ navigation }) => {

    const [loading, setLoading] = useState(false);
    const [fullname, setFullname] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    var SharedPreferences = require("react-native-shared-preferences");

    const onRegister = async () => {
        try {
            setLoading(true)
            const response = await fetch('http://task-service.fun/api/register', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: fullname,
                    email: email,
                    phone_number: phone,
                    password: password
                })
            });
            const json = await response.json()

            if (json.code == 200) {
                if (json.data.role === "USER")
                    SharedPreferences.setItem("isUser", "true");
                else
                    SharedPreferences.setItem("isUser", "false");

                SharedPreferences.setName("credential");
                SharedPreferences.setItem("token", json.data.remember_token);
                SharedPreferences.setItem("user", JSON.stringify(json.data));
                navigation.navigate("MainPage", { user: json.data })
            } else {
                console.log('failed register : ' + json.message)
                Toast.show(json.message, Toast.LONG);
            }
        } catch (error) {
            console.log(`error ${error} runtime type ${typeof (error)}`)
        } finally {
            setLoading(false)
        }
    };

    return (
        <View style={styles.container}>
            <Text style={[styles.title, styles.leftTitle]}>Create new account</Text>
            <View style={styles.InputContainer}>
                <TextInput
                    style={styles.body}
                    placeholder="Full Name"
                    onChangeText={setFullname}
                    value={fullname}
                    placeholderTextColor={AppStyles.color.grey}
                    underlineColorAndroid="transparent"
                />
            </View>
            <View style={styles.InputContainer}>
                <TextInput
                    style={styles.body}
                    placeholder="Phone Number"
                    onChangeText={setPhone}
                    value={phone}
                    placeholderTextColor={AppStyles.color.grey}
                    underlineColorAndroid="transparent"
                />
            </View>
            <View style={styles.InputContainer}>
                <TextInput
                    style={styles.body}
                    placeholder="E-mail Address"
                    onChangeText={setEmail}
                    value={email}
                    placeholderTextColor={AppStyles.color.grey}
                    underlineColorAndroid="transparent"
                />
            </View>
            <View style={styles.InputContainer}>
                <TextInput
                    style={styles.body}
                    placeholder="Password"
                    secureTextEntry={true}
                    onChangeText={setPassword}
                    value={password}
                    placeholderTextColor={AppStyles.color.grey}
                    underlineColorAndroid="transparent"
                />
            </View>
            <TouchableOpacity
                style={[styles.facebookContainer, { marginTop: 50 }]}
                onPress={() => onRegister()}>
                {loading ? <ActivityIndicator /> : <Text style={styles.facebookText}>Sign Up</Text>}
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('LoginPage')}>
                <Text style={styles.signInText}>
                    Sign in
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    title: {
        fontSize: AppStyles.fontSize.title,
        fontWeight: 'bold',
        color: AppStyles.color.tint,
        marginTop: 20,
        marginBottom: 20,
    },
    leftTitle: {
        alignSelf: 'stretch',
        textAlign: 'left',
        marginLeft: 20,
    },
    content: {
        paddingLeft: 50,
        paddingRight: 50,
        textAlign: 'center',
        fontSize: AppStyles.fontSize.content,
        color: AppStyles.color.text,
    },
    loginContainer: {
        width: AppStyles.buttonWidth.main,
        backgroundColor: AppStyles.color.tint,
        borderRadius: AppStyles.borderRadius.main,
        padding: 10,
        marginTop: 30,
    },
    loginText: {
        color: AppStyles.color.white,
    },
    placeholder: {
        color: 'red',
    },
    InputContainer: {
        width: AppStyles.textInputWidth.main,
        marginTop: 30,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: AppStyles.color.grey,
        borderRadius: AppStyles.borderRadius.main,
    },
    body: {
        height: 42,
        paddingLeft: 20,
        paddingRight: 20,
        color: AppStyles.color.text,
    },
    facebookContainer: {
        alignItems: 'center',
        width: AppStyles.buttonWidth.main,
        backgroundColor: AppStyles.color.tint,
        borderRadius: AppStyles.borderRadius.main,
        padding: 10,
        marginTop: 30,
    },
    facebookText: {
        color: AppStyles.color.white,
    },
    signInText: {
        marginTop: 10,
        color: Colors.blue500
    }
});

export default RegisterPage;