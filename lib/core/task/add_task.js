import React, { useEffect, useCallback, useState } from 'react';
import { Appbar } from 'react-native-paper';
import {
    StyleSheet,
    Text,
    TextInput,
    Image,
    View,
    ActivityIndicator,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import { AppStyles } from '../../app_styles';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as ImagePicker from "react-native-image-picker";
import ImgToBase64 from "react-native-image-base64";
import Toast from 'react-native-simple-toast';
import { ScrollView } from 'react-native-gesture-handler';


const AddTaskPage = ({ navigation }) => {
    let uri = null;
    const [pickerResponse, setPickerResponse] = useState(null);

    const [isLoading, setLoading] = useState(false);
    const [token, setToken] = useState('');
    const [name, setName] = useState('');
    const [notes, setNotes] = useState('');
    var SharedPreferences = require("react-native-shared-preferences");
    const [mydate, setDate] = useState(new Date());
    const [startTime, setStartTime] = useState(new Date());
    const [endTime, setEndTime] = useState(new Date());
    const [isDisplayDate, setDisplayDate] = useState(false);
    const [isDisplayStartTime, setDisplayStartTime] = useState(false);
    const [isDisplayEndTime, setDisplayEndTime] = useState(false);

    const changeSelectedDate = (event, selectedDate) => {
        const currentDate = selectedDate || mydate;
        setDate(currentDate);
        setDisplayDate(false)
        console.log("localdate " + mydate.toLocaleDateString())
    };

    const displayDatepicker = () => {
        setDisplayDate(true);
    };

    const changeStartTime = (event, selectedDate) => {
        const currentDate = selectedDate || mydate;
        setStartTime(currentDate);
        setDisplayStartTime(false)
    };

    const displayStartTime = () => {
        setDisplayStartTime(true);
    };

    const changeEndTime = (event, selectedDate) => {
        const currentDate = selectedDate || mydate;
        setEndTime(currentDate);
        setDisplayEndTime(false)
    };

    const displayEndTime = () => {
        setDisplayEndTime(true);
    };

    const generateBase64Image = (uriImage) => {
        if (uriImage != null) {
            ImgToBase64.getBase64String(uriImage)
                .then(base64String => {
                    console.log("image generated : " + base64String.substring(0, 4));
                    reqApi(base64String);
                })
                .catch(err => console.log("error " + err));
        } else {
            writeUserData(packageImage);
        }
    };
    const onImageLibraryPress = useCallback(() => {
        //    setVisible(false);
        const options = {
            selectionLimit: 1,
            mediaType: "photo",
            includeBase64: false
        };
        ImagePicker.launchImageLibrary(options, setPickerResponse);
    }, []);

    if (pickerResponse != null) {
        console.log("pickerResponse.assets : " + pickerResponse.assets);
        console.log(
            "pickerResponse.assets[0].uri : " + pickerResponse.assets[0].uri
        );
        uri = pickerResponse.assets && pickerResponse.assets[0].uri;
    }

    const onPressAdd = async () => {
        try {
            setLoading(true)
            generateBase64Image(uri);
        } catch (error) {
            console.log(`error ${error} runtime type ${typeof (error)}`)
        } finally {
            setLoading(false);
        }
    }

    const reqApi = async (base64String) => {
        try {
            console.log("reqApi : " + base64String.substring(0, 4));
            //    console.log("token : " + token);
            var headers = {
                'Authorization': 'Bearer ' + token,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            };

            const response = await fetch('http://task-service.fun/api/add_task', {
                method: 'POST',
                headers: headers,
                body: JSON.stringify({
                    name: name,
                    notes: notes,
                    date: mydate.toLocaleDateString(),
                    start_time: startTime.toLocaleTimeString(),
                    end_time: endTime.toLocaleTimeString(),
                    image: base64String
                })
            });
            const json = await response.json()
            if (json.code == 200) {
                console.log("resp " + JSON.stringify(json))
                console.log("header " + JSON.stringify(headers))
                navigation.navigate("MainPage")
            } else {
                console.log("resp " + json.message);
                Toast.show(json.message, Toast.LONG);
            }
        } catch (error) {s
            console.log(`error ${error} runtime type ${typeof (error)}`)
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        SharedPreferences.setName("credential");
        SharedPreferences.getItem("token", function (value) {
            setToken(value)
        });
    }, []);

    return (
        <View style={{ flex: 1 }}>
            <Appbar.Header style={{ backgroundColor: AppStyles.color.tint }}>
                <Appbar.BackAction onPress={() => { navigation.goBack() }} color={AppStyles.color.white} />
                <Appbar.Content title="Add Task" titleStyle={{ color: AppStyles.color.white }} />
            </Appbar.Header>

            <ScrollView>
                <View style={styles.container}>
                    <ImageBackground style={styles.imageBackground}>
                        <Image
                            style={{
                                height: 200,
                                width: 200,
                                overflow: "visible",
                                marginBottom: 20
                            }}
                            source={
                                uri != undefined
                                    ? { uri: uri }
                                    : require("../../../assets/default.jpeg")
                            }
                        />
                    </ImageBackground>
                    <TouchableOpacity
                        mode="contained"
                        onPress={onImageLibraryPress}
                        style={[styles.buttonStyle, { marginBottom: 30 }]}
                    >
                        <Text style={styles.txtStyleButton}>Change Photo</Text>
                    </TouchableOpacity>
                    <View style={styles.InputContainer}>
                        <TextInput
                            style={styles.body}
                            placeholder="Name"
                            onChangeText={setName}
                            value={name}
                            placeholderTextColor={AppStyles.color.grey}
                            underlineColorAndroid="transparent"
                        />
                    </View>
                    <View style={styles.InputContainer}>
                        <TextInput
                            style={styles.body}
                            placeholder="Notes"
                            onChangeText={setNotes}
                            value={notes}
                            placeholderTextColor={AppStyles.color.grey}
                            underlineColorAndroid="transparent"
                        />
                    </View>

                    <Text style={{ marginTop: 20, marginBottom: 10, color: 'black' }}>Select Date</Text>
                    {isDisplayDate ? <DateTimePicker
                        testID="dateTimePicker"
                        value={mydate}
                        mode={'date'}
                        is24Hour={true}
                        display="default"
                        onChange={changeSelectedDate}
                    /> : <TouchableOpacity
                        onPress={() => displayDatepicker()}
                        style={{
                            paddingHorizontal: 20,
                            paddingVertical: 10,
                            borderWidth: 1,
                            borderStyle: 'solid',
                            borderColor: AppStyles.color.grey,
                            borderRadius: 12,
                            width: AppStyles.buttonWidth.main,
                            alignContent: 'center',
                            alignItems: 'center'
                        }}>
                        <Text style={{ color: 'black' }}>{mydate.toLocaleDateString()}</Text>
                    </TouchableOpacity>}

                    <Text style={{ marginTop: 20, marginBottom: 10, color: 'black' }}>Select Start Time</Text>
                    {isDisplayStartTime ? <DateTimePicker
                        testID="dateTimePicker"
                        value={startTime}
                        mode={'time'}
                        is24Hour={true}
                        display="default"
                        onChange={changeStartTime}
                    /> : <TouchableOpacity
                        onPress={() => displayStartTime()}
                        style={{

                            paddingHorizontal: 20,
                            paddingVertical: 10,
                            borderWidth: 1,
                            borderStyle: 'solid',
                            borderColor: AppStyles.color.grey,
                            borderRadius: 12,
                            width: AppStyles.buttonWidth.main,
                            alignContent: 'center',
                            alignItems: 'center'
                        }}>
                        <Text style={{ color: 'black' }}>{startTime.toLocaleTimeString()}</Text>
                    </TouchableOpacity>}

                    <Text style={{ marginTop: 20, marginBottom: 10, color: 'black' }}>Select End Time</Text>
                    {isDisplayEndTime ? <DateTimePicker
                        testID="dateTimePicker"
                        value={endTime}
                        mode={'time'}
                        is24Hour={true}
                        display="default"
                        onChange={changeEndTime}
                    /> : <TouchableOpacity
                        onPress={() => displayEndTime()}
                        style={{
                            paddingHorizontal: 20,
                            paddingVertical: 10,
                            borderWidth: 1,
                            borderStyle: 'solid',
                            borderColor: AppStyles.color.grey,
                            borderRadius: 12,
                            width: AppStyles.buttonWidth.main,
                            alignContent: 'center',
                            alignItems: 'center'
                        }}>
                        <Text style={{ color: 'black' }}>{endTime.toLocaleTimeString()}</Text>
                    </TouchableOpacity>}

                    <TouchableOpacity
                        style={[styles.loginContainer, { marginBottom: 30 }]}
                        onPress={() => onPressAdd()}>
                        {isLoading ? <ActivityIndicator /> : <Text style={styles.loginText}>Add</Text>}
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    title: {
        fontSize: AppStyles.fontSize.title,
        fontWeight: 'bold',
        color: AppStyles.color.tint,
        marginTop: 20,
        marginBottom: 20,
    },
    leftTitle: {
        alignSelf: 'stretch',
        textAlign: 'left',
        marginLeft: 20,
    },
    content: {
        paddingLeft: 50,
        paddingRight: 50,
        textAlign: 'center',
        fontSize: AppStyles.fontSize.content,
        color: AppStyles.color.text,
    },
    loginContainer: {
        alignItems: 'center',
        width: AppStyles.buttonWidth.main,
        backgroundColor: AppStyles.color.tint,
        borderRadius: AppStyles.borderRadius.main,
        padding: 10,
        marginTop: 30,
    },
    loginText: {
        color: AppStyles.color.white,
    },
    placeholder: {
        color: 'red',
    },
    InputContainer: {
        width: AppStyles.textInputWidth.main,
        marginTop: 30,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: AppStyles.color.grey,
        borderRadius: AppStyles.borderRadius.main,
    },
    body: {
        height: 42,
        paddingLeft: 20,
        paddingRight: 20,
        color: AppStyles.color.text,
    },
    buttonStyle: {
        marginRight: 24,
        marginLeft: 24,
        marginTop: 10,
        marginBottom: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#009EFF',
    },
    txtStyleButton: {
        color: '#009EFF',
        width: 250,
        textAlign: 'center',
    },
});

export default AddTaskPage