import React, { useEffect, useState } from 'react';
import { Appbar } from 'react-native-paper';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ActivityIndicator,
    TouchableOpacity,
} from 'react-native';
import { AppStyles } from '../../app_styles';

const UpdateTaskPage = ({ route, navigation }) => {

    const data = route.params.detailTask;

    const [isLoading, setLoading] = useState(false);
    const [token, setToken] = useState('');
    const [name, setName] = useState(data.name);
    const [notes, setNotes] = useState(data.notes);
    var SharedPreferences = require("react-native-shared-preferences");

    const onPressUpdate = async () => {
        try {
            setLoading(true)
            var headers = {
                'Authorization': 'Bearer ' + token,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            };
            const response = await fetch('http://task-service.fun/api/update_task', {
                method: 'POST',
                headers: headers,
                body: JSON.stringify({
                    id: data.id.toString(),
                    name: name,
                    notes: notes,
                })
            });
            const json = await response.json()
            if (json.code == 200) {
                console.log("resp " + JSON.stringify(json))
                console.log("headr " + JSON.stringify(headers))
                navigation.navigate("TaskPage")
            } else {
                Toast.show(json.message, Toast.LONG);
            }
        } catch (error) {
            console.log(`error ${error} runtime type ${typeof (error)}`)
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        SharedPreferences.setName("credential");
        SharedPreferences.getItem("token", function (value) {
            setToken(value)
        });
    }, []);

    return (
        <View style={{ flex: 1 }}>
            <Appbar.Header style={{ backgroundColor: AppStyles.color.tint }}>
                <Appbar.BackAction onPress={() => { navigation.goBack() }} color={AppStyles.color.white} />
                <Appbar.Content title="Update Task" titleStyle={{ color: AppStyles.color.white }} />
            </Appbar.Header>
            <View style={styles.container}>
                <View style={styles.InputContainer}>
                    <TextInput
                        style={styles.body}
                        placeholder="Name"
                        onChangeText={setName}
                        value={name}
                        placeholderTextColor={AppStyles.color.grey}
                        underlineColorAndroid="transparent"
                    />
                </View>
                <View style={styles.InputContainer}>
                    <TextInput
                        style={styles.body}
                        placeholder="Notes"
                        onChangeText={setNotes}
                        value={notes}
                        placeholderTextColor={AppStyles.color.grey}
                        underlineColorAndroid="transparent"
                    />
                </View>
                <TouchableOpacity
                    style={styles.loginContainer}
                    onPress={() => onPressUpdate()}>
                    {isLoading ? <ActivityIndicator /> : <Text style={styles.loginText}>Update</Text>}
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    title: {
        fontSize: AppStyles.fontSize.title,
        fontWeight: 'bold',
        color: AppStyles.color.tint,
        marginTop: 20,
        marginBottom: 20,
    },
    leftTitle: {
        alignSelf: 'stretch',
        textAlign: 'left',
        marginLeft: 20,
    },
    content: {
        paddingLeft: 50,
        paddingRight: 50,
        textAlign: 'center',
        fontSize: AppStyles.fontSize.content,
        color: AppStyles.color.text,
    },
    loginContainer: {
        alignItems: 'center',
        width: AppStyles.buttonWidth.main,
        backgroundColor: AppStyles.color.tint,
        borderRadius: AppStyles.borderRadius.main,
        padding: 10,
        marginTop: 30,
    },
    loginText: {
        color: AppStyles.color.white,
    },
    placeholder: {
        color: 'red',
    },
    InputContainer: {
        width: AppStyles.textInputWidth.main,
        marginTop: 30,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: AppStyles.color.grey,
        borderRadius: AppStyles.borderRadius.main,
    },
    body: {
        height: 42,
        paddingLeft: 20,
        paddingRight: 20,
        color: AppStyles.color.text,
    },
    buttonStyle: {
        marginRight: 24,
        marginLeft: 24,
        marginTop: 10,
        marginBottom: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#009EFF',
    },
    txtStyleButton: {
        color: '#009EFF',
        width: 250,
        textAlign: 'center',
    },
});

export default UpdateTaskPage