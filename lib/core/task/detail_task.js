import React, { useEffect, useState } from 'react';
import { Appbar } from 'react-native-paper';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    KeyboardAvoidingView
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { AppStyles } from '../../app_styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ReportItem from '../../components/report_item';


const DetailTaskPage = ({ route, navigation }) => {

    const data = route.params.detailTask;
    console.log("data " + JSON.stringify(data))
    const [token, setToken] = useState('');
    const [comment, setComment] = useState('');
    const [isUser, setIsUser] = useState(true);
    var SharedPreferences = require("react-native-shared-preferences");


    // const onPressAddComment = async () => {
    //     try {
    //         var headers = {
    //             'Authorization': 'Bearer ' + token,
    //             Accept: 'application/json',
    //             'Content-Type': 'application/json'
    //         };
    //         const response = await fetch('http://task-service.fun/api/add_comment', {
    //             method: 'POST',
    //             headers: headers,
    //             body: JSON.stringify({
    //                 task_id: data.id.toString(),
    //                 content: comment
    //             })
    //         });
    //         const json = await response.json()
    //         console.log("resp " + JSON.stringify(json))
    //         console.log("headr " + JSON.stringify(headers))
    //         navigation.navigate("TaskPage")
    //     } catch (error) {
    //         console.log(`error ${error} runtime type ${typeof (error)}`)
    //     }
    // }

    const onPressUpdate = () => {
        navigation.navigate("UpdateTaskPage", { detailTask: data })
    }

    const onPressDelete = async (id) => {
        try {
            var headers = {
                'Authorization': 'Bearer ' + token,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            };
            const response = await fetch('http://task-service.fun/api/delete_task', {
                method: 'POST',
                headers: headers,
                body: JSON.stringify({
                    id: id
                })
            });
            const json = await response.json()
            if (json.code == 200) {
                console.log(`response ${JSON.stringify(json)}`)
                navigation.navigate('TaskPage')
            } else {
                Toast.show(json.message, Toast.LONG);
            }
        } catch (error) {
            console.log(`error ${error} runtime type ${typeof (error)}`)
        }
    }

    useEffect(() => {
        SharedPreferences.setName("credential");
        SharedPreferences.getItem("token", function (value) {
            setToken(value)
        });
        SharedPreferences.getItem("isUser", function (value) {
            var isTrueSet = (value === 'true');
            setIsUser(isTrueSet)
        })
    }, []);

    return (
        <View style={{ flex: 1 }}>
            <Appbar.Header style={{ backgroundColor: AppStyles.color.tint }}>
                <Appbar.BackAction onPress={() => { navigation.goBack() }} color={AppStyles.color.white} />
                <Appbar.Content title={data.name} titleStyle={{ color: AppStyles.color.white }} />
                {isUser ? <Appbar.Action icon="square-edit-outline" onPress={() => onPressUpdate()} color={AppStyles.color.white} /> : null}
                {isUser ? <Appbar.Action icon="trash-can-outline" onPress={() => onPressDelete(data.id.toString())} color={AppStyles.color.white} /> : null}
            </Appbar.Header>
            <View style={styles.container}>
                <View style={{ flex: 1 }}>
                    <View style={{ marginTop: 15 }}><Text style={styles.subTitle}>Tanggal Kegiatan</Text></View>
                    <View style={{ marginTop: 5 }}><Text style={styles.regular}>{data.date}</Text></View>
                    <View style={{ flexDirection: 'row', marginTop: 20, flex: 2 }}>
                        <View style={{ flexDirection: 'column', flex: 1 }}>
                            <View style={{}}><Text style={styles.subTitle}>Waktu Mulai</Text></View>
                            <View style={{ marginTop: 5 }}><Text style={styles.regular}>{data.start_time}</Text></View>
                        </View>
                        <View style={{ flexDirection: 'column', flex: 1 }}>
                            <View style={{}}><Text style={styles.subTitle}>Waktu Selesai</Text></View>
                            <View style={{ marginTop: 5 }}><Text style={styles.regular}>{data.end_time}</Text></View>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 2 }}>
                    <View style={{}}><Text style={styles.subTitle}>Laporan:</Text></View>
                    <FlatList style={{ marginLeft: 12, marginTop: 7 }} data={data.reports}
                        keyExtractor={({ id }, index) => id}
                        renderItem={({ item }) => (

                            <TouchableOpacity onPress={() => navigation.navigate("DetailReportPage", { detailReport: item })}>
                                <ReportItem data={item} />
                            </TouchableOpacity>

                            //                            <View style={{ flexDirection: 'row', flex: 2, alignSelf: 'baseline', alignItems: 'center', justifyContent: "center", marginBottom: 10 }}>
                            //                                <Text style={[{ marginRight: 10, fontWeight: 'bold', fontSize: 12, color: 'black' }]}>[{item.name}]</Text>
                            //                                <Text style={{ fontSize: 12, color: 'black' }}>{item.description}</Text>
                            //                            </View>

                        )}
                    />
                </View>
                {/* <View>
                    <KeyboardAvoidingView
                        behavior={Platform.OS === "ios" ? "padding" : "height"}
                        style={styles.writeTaskWrapper}
                    >
                        <TextInput style={[{ flex: 1, marginRight: 20 }, styles.input]} placeholder={'type comment..'} value={comment} onChangeText={text => setComment(text)} />
                        <TouchableOpacity onPress={() => onPressAddComment()}>
                            <Ionicons name='send' size={20} />
                        </TouchableOpacity>
                    </KeyboardAvoidingView>
                </View> */}
                {isUser ? <TouchableOpacity
                    mode="contained"
                    onPress={() => navigation.navigate('AddReportPage', { detailTask: data })}
                    style={styles.buttonStyle}
                ><Text style={styles.txtStyleButton}>Tambah Laporan</Text></TouchableOpacity> : null}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 4,
        backgroundColor: '#E8EAED',
        padding: 16,
        flexDirection: 'column'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18
    },
    subTitle: {
        fontWeight: '500',
        fontSize: 16,
        color: 'black'
    },
    regular: {
        fontSize: 14,
        color: 'black'
    },

    buttonStyle: {
        marginRight: 24,
        marginLeft: 24,
        marginTop: 10,
        marginBottom: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: AppStyles.color.tint,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: AppStyles.color.tint,
    },
    txtStyleButton: {
        color: AppStyles.color.white,
        width: 250,
        textAlign: 'center',
    },
    buttonStyleOutline: {
        marginRight: 24,
        marginLeft: 24,
        marginBottom: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: AppStyles.color.white,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: AppStyles.color.tint,
    },
    txtStyleButtonOutline: {
        color: AppStyles.color.tint,
        width: 250,
        textAlign: 'center',
    },
    writeTaskWrapper: {
        padding: 10,
        position: 'absolute',
        bottom: 10,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        paddingVertical: 5,
        paddingHorizontal: 20,
        backgroundColor: '#FFF',
        borderRadius: 10,
        borderColor: '#C0C0C0',
        borderWidth: 1,
    },
    addWrapper: {
        width: 30,
        height: 30,
        backgroundColor: '#FFF',
        borderRadius: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#C0C0C0',
        marginLeft: 10,
        borderWidth: 1,
    },
    addText: {},
});

export default DetailTaskPage