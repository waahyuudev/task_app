import React, { useEffect, useState } from 'react';
import { useIsFocused } from "@react-navigation/native";
import { Appbar } from 'react-native-paper';
import {
    StyleSheet,
    Text,
    View,
    ActivityIndicator,
    TouchableOpacity,
    FlatList
} from 'react-native';
import { AppStyles } from '../../app_styles';
import TaskItem from '../../components/task_item';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import Toast from 'react-native-simple-toast';
import RNFS from 'react-native-fs';


const TaskPage = ({ route, navigation, props }) => {

    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const [isUser, setIsUser] = useState(true);
    var SharedPreferences = require("react-native-shared-preferences");
    const isFocused = useIsFocused();

    const exportPdf = async () => {
        try {
            var tdTask = data.map((item) => {
                return `<tr>
                    <td>${item.name}</td>
                    <td>${item.date}</td>
                    <td>${item.start_time}</td>
                    <td>${item.end_time}</td>
                    <td>${item.notes}</td>
                </tr>`
            })
            const html = `<html>
            <head>
            <style>
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }
            
            td, th {
              border: 1px solid #dddddd;
              text-align: left;
              padding: 8px;
            }
            
            tr:nth-child(even) {
              background-color: #dddddd;
            }
            </style>
            </head>
            <body>
            
            <h2>Task List</h2>
            
            <table>
              <tr>
                <th>Name</th>
                <th>Date</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Notes</th>
              </tr>
              ${tdTask}
            </table>
            
            </body>
            </html>
            `;
            var today = Math.round((new Date()).getTime() / 1000);
            const options = {
                html,
                fileName: "test",
                directory: 'Documents'
            };
            const file = await RNHTMLtoPDF.convert(options);
            console.log(`path pdf ${file.filePath}`)
            console.log(`path RNFS.DownloadDirectoryPath ${RNFS.DownloadDirectoryPath}`)

            await RNFS.copyFile(file.filePath, RNFS.DownloadDirectoryPath + "/" + `TASK_${today}.pdf`)
            Toast.show("Berhasil Create PDF", Toast.LONG);
        } catch (error) {
            Toast.show(error, Toast.LONG);
            console.log(`error ${error}`)
        } finally {

        }
    }

    const getTask = async (token) => {
        try {
//            console.log("token : " + token);

            var headers = {
                'Authorization': 'Bearer ' + token
            };
            const response = await fetch('http://task-service.fun/api/list_task', {
                method: 'POST',
                headers: headers
            });

            const json = await response.json();

            setData(json.data);
            console.log("response list task " + JSON.stringify(json.data[0]))
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        SharedPreferences.setName("credential");
        SharedPreferences.getItem("isUser", function(value){
            var isTrueSet = (value === 'true');
            setIsUser(isTrueSet)
        })
        SharedPreferences.getItem("token", function (value) {
//            console.log("token " + value)
            if (isFocused)
                getTask(value)
        });
    }, [props, isFocused]);

    return (
        <View style={{ flex: 1 }}>
            <Appbar.Header style={{ backgroundColor: AppStyles.color.tint }}>
                <Appbar.Content title="Kegiatan Harian" titleStyle={{ color: AppStyles.color.white }} />
                <Appbar.Action icon="file-export" onPress={() => exportPdf()} color={AppStyles.color.white} />
            </Appbar.Header>
            <View style={styles.container}>
                <View style={{ padding: 24, flex: 1 }}>
                    {isLoading ? <ActivityIndicator /> : (
                        data.length == 0 ? <Text style={{ flex: 1, alignSelf: 'center', textAlignVertical: 'center', color: 'black' }}>Task Empty</Text> : <FlatList
                            data={data}
                            keyExtractor={({ id }, index) => id}
                            renderItem={({ item }) => (

                                <TouchableOpacity onPress={() => navigation.navigate("DetailTaskPage", { detailTask: item })}>
                                    <TaskItem data={item} />
                                </TouchableOpacity>

                            )}
                        />
                    )}
                </View>
                {isUser ? <TouchableOpacity mode="contained" onPress={() => navigation.navigate('AddTaskPage')}
                    style={styles.buttonStyle}>
                    <Text style={styles.txtStyleButton}>Tambah Kegiatan</Text>
                    </TouchableOpacity> :
                    <View></View>}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E8EAED',
    },
    buttonStyle: {
        marginRight: 24,
        marginLeft: 24,
        marginTop: 10,
        marginBottom: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: AppStyles.color.tint,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: AppStyles.color.tint,
    },
    txtStyleButton: {
        color: AppStyles.color.white,
        width: 250,
        textAlign: 'center',
    },
});

export default TaskPage;