import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { LoginPage, RegisterPage, MainPage, TaskPage, AddTaskPage, DetailTaskPage, UpdateTaskPage, AddReportPage, DetailReportPage, UpdateProfilePage } from "./core";

const Stack = createStackNavigator();

const RouteNavigation = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator
                initialRouteName="LoginPage"
                screenOptions={{
                    headerShown: false
                }}
            >
                <Stack.Screen name="LoginPage" component={LoginPage} />
                <Stack.Screen name="RegisterPage" component={RegisterPage} />
                <Stack.Screen name="MainPage" component={MainPage} />
                <Stack.Screen name="TaskPage" component={TaskPage} />
                <Stack.Screen name="AddTaskPage" component={AddTaskPage} />
                <Stack.Screen name="DetailTaskPage" component={DetailTaskPage} />
                <Stack.Screen name="DetailReportPage" component={DetailReportPage} />
                <Stack.Screen name="UpdateTaskPage" component={UpdateTaskPage} />
                <Stack.Screen name="AddReportPage" component={AddReportPage} />
                <Stack.Screen name="UpdateProfilePage" component={UpdateProfilePage} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default RouteNavigation;