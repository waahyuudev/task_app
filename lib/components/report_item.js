import React from 'react';
import { View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    ImageBackground } from 'react-native';
import { Colors } from 'react-native-paper';

const ReportItem = (props) => {

    return (
        <View style={styles.item}>
            <View style={styles.itemLeft}>
                    <ImageBackground style={styles.imageBackground}>
                        <Image style={{
                            height: 50,
                            width: 50,
                            overflow: "visible",
                            marginBottom: 20
                            }}
                            source={props.data.image === "null"
                                ? { uri: require("../../assets/profile.png") }
                                : {uri:props.data.image}}
                        />
                    </ImageBackground>
                <View style={{ flexDirection: 'column', flex: 1 }}>
                    <Text style={styles.itemText}>{props.data.name}</Text>
                    <Text style={{ fontSize: 12, color: 'black' }}>{ "Desc : " + props.data.description}</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#FFF',
        padding: 15,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 20,
    },
    itemLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    square: {
        width: 24,
        height: 24,
        backgroundColor: '#55BCF6',
        opacity: 0.4,
        borderRadius: 5,
        marginRight: 15,
    },
    itemText: {
        maxWidth: '80%',
        color: 'black'
    },
});

export default ReportItem